<?php

namespace Lib;


class Response
{
    private $status  = 200;
    private $headers = [];
    private $body;

    public function raw($body) {
        $this->body = $body;
        return $this;
    }

    public function json($data) {
        $this->addHeader('Content-type', 'application/json');
        $this->body = json_encode($data);
        return $this;
    }

    public function addHeader($key, $value) {
        array_push($this->headers, "{$key}: {$value}");
        return $this;
    }

    public function execute() {
        foreach($this->headers as $header) {
            header($header);
        }
        http_response_code($this->status);
        return $this->body;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

}
<?php

namespace Lib;


class Router
{
    private $path;

    public function __construct($path)
    {
        $this->path = explode('/', $path);
    }

    private function getBody()
    {
        if (@$_SERVER["CONTENT_TYPE"] == 'application/json') {
            return json_decode(file_get_contents('php://input'), true) ?? null;
        } elseif (@$_SERVER['CONTENT_TYPE'] == 'application/x-www-form-urlencoded') {
            return $_POST;
        } else {
            return null;
        }
    }

    private function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function getQuery()
    {
        $id = $this->path[1] ?? null;
        unset($_GET['path']);
        return array_merge([ 'id' => $id ], $_GET);
    }

    private function getController()
    {
        $resource = $this->path[0];
        $controller = '\Controllers\\' . ucfirst($resource) . 'Controller';
        return new $controller;
    }

    public function run()
    {
        return call_user_func_array(
            [ $this->getController(), $this->getMethod() ],
            [ $this->getQuery(), $this->getBody() ]
        );
    }

}
<?php


namespace Lib;

use \PDO;
use \PDOException;


class PDOConnection
{
    private static $instance;
    private static $config;

    /**
     * Returns PDO instance if exists or creates new PDO connection if not
     *
     * @return PDO
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::connect();
        }
        return self::$instance;
    }

    private static function connect()
    {
        try {
            $config = self::$config;
            self::$instance = new PDO(
                "mysql:host={$config['host']};dbname={$config['dbname']}",
                $config['username'],
                $config['password'],
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES => false
                ]
            );
        } catch (PDOException $e) {
            die('Database error: ' . $e->getMessage());
        }
    }

    /**
     * @param $config array
     */

    public static function setConfig($config)
    {
        self::$config = $config;
    }

}
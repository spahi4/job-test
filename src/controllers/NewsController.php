<?php

namespace Controllers;

use \Lib\Response;
use \Lib\PDOConnection;


class NewsController extends RestBaseController
{
    public function GET($query)
    {
        $offset = (int) ($query['offset'] ?? 0);
        $perPage = 10;
        return PDOConnection::getInstance()
            ->query("SELECT * FROM news ORDER BY id DESC LIMIT {$offset},{$perPage}")
            ->fetchAll();
    }

    public function POST($query, $body) {
        $result = PDOConnection::getInstance()
            ->prepare("INSERT INTO news (title, content, tags, created_at) VALUES (:title, :content, :tags, NOW())")
            ->execute([
                'title' => $body['title'],
                'content' => $body['content'],
                'tags' => @$body['tags']
            ]);
        if ($result) {
            // return last inserted item if success
            $id = PDOConnection::getInstance()->lastInsertId();
            $item = PDOConnection::getInstance()
                ->query("SELECT * FROM news WHERE id = {$id}")
                ->fetch();
            return (new Response())->setStatus(201)->json($item);
        } else {
            return (new Response())->setStatus(500);
        }
    }

    public function PUT($query, $body) {
        $result = PDOConnection::getInstance()
            ->prepare("UPDATE news SET title = :title, content = :content, tags = :tags WHERE id = :id")
            ->execute([
                'id' => $query['id'],
                'title' => $body['title'],
                'content' => $body['content'],
                'tags' => @$body['tags']
            ]);
        if ($result) {
            return (new Response())->setStatus(204);
        } else {
            return (new Response())->setStatus(404);
        }
    }

    public function DELETE($query) {
        $result = PDOConnection::getInstance()
            ->prepare("DELETE FROM news WHERE id = :id")
            ->execute([ 'id' => $query['id'] ]);
        if ($result) {
            return (new Response())->setStatus(204);
        } else {
            return (new Response())->setStatus(404);
        }
    }

}
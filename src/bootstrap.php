<?php

spl_autoload_register(function ($class) {
    $base_dir = __DIR__ . '/';
    $file = $base_dir . str_replace('\\', '/', $class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});

use \Lib\Response;
use \Lib\PDOConnection;
use \Lib\Router;

$config = require_once  __DIR__ . '/config.php';

PDOConnection::setConfig($config['database']);

$router = new Router($_GET['path']);

$response = $router->run();

// magic return
if (!($response instanceof Response)) {
    $instance = new Response();
    if (is_object($response) || is_array($response)) {
        $response = $instance->json($response);
    } else {
        $response = $instance->raw($response);
    }
}

echo $response->execute();
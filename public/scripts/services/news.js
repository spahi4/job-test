import { apiUrl } from '../utils/config';

class NewsService {

    items = []

    finished = false

    loading = false

    constructor($http) {
        "ngInject";
        this._$http = $http;
    }

    loadItems() {
        if (this.finished) return;
        return this._$http({
            method: 'GET',
            url: `${apiUrl}/news`,
            params: { offset: this.items.length }
        }).then(response => {
            return new Promise(resolve => {
                setTimeout(() => {
                    if (response.data.length === 0) {
                        this.finished = true;
                    }
                    this.loading = false;
                    this.items.push(...response.data);
                    resolve();
                }, 500);
            }); // emulate delay
        }).catch(response => {
            console.error(response);
        });
    }

    getItems() {
        return this.items;
    }

    getLoading() {
        return this.loading;
    }

    getFinished() {
        return this.finished;
    }

    deleteItem(id) {
        this._$http({
            method: 'DELETE',
            url: `${apiUrl}/news/${id}`
        });
        let index = this.items.find(i => i.id === id);
        this.items.splice(index, 1);
    }

    updateItem({ id, ...item }) {
        console.log(id, item);
        this._$http({
            method: 'PUT',
            url: `${apiUrl}/news/${id}`,
            data: item,
            headers: { 'Content-Type': 'application/json' }
        });
        let index = this.items.find(i => i.id === id);
        this.items[index] = item;
    }

    createItem(item) {
        this._$http({
            method: 'POST',
            url: `${apiUrl}/news`,
            data: item,
            headers: { 'Content-Type': 'application/json' }
        }).then(response => {
            this.items.unshift(response.data);
        });
    }

}

export default NewsService;
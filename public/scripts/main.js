import angular from 'angular';

import AppComponent from './components/app/app.component';
import DialogComponent from './components/create-dialog/dialog.component';
import ContentComponent from './components/content/content.component';

import NewsService from './services/news';

angular.module('app', [])
.component('app', AppComponent)
.component('createDialog', DialogComponent)
.component('content', ContentComponent)
.service('NewsService', NewsService);
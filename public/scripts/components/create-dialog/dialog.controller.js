class dialogController {

    item = {}

    constructor($rootScope, $element) {
        "ngInject";
        this._$rootScope = $rootScope;
        this.dialog = $element[0].querySelector('#dialog');
        this.form = $element[0].querySelector('form');
        $rootScope.$on("open-dialog", ::this.open);
    }

    open(event, item) {
        this.item = item;
        this.isNew = !item;
        this.dialog.show();
    }

    close() {
        this.dialog.close();
    }

    submit() {
        this._$rootScope.$emit(this.isNew ? 'news-created' : 'news-updated', this.item);
        this.close();
    }
}

export default dialogController;
import template from './dialog.template.html';
import controller from './dialog.controller';
import './dialog.styles.scss';

let dialogComponent = {
    template,
    controller,
    replace: true
};

export default dialogComponent;
import angular from 'angular';

class contentController {

    constructor($scope, $window, $timeout, $rootElement, $rootScope, NewsService) {
        "ngInject";
        this._NewsService = NewsService;
        this._$scope = $scope;
        this._$window = $window;
        this._$rootElement = $rootElement[0];
        this._$rootScope = $rootScope;
        this._$timeout = $timeout;
        this.items = NewsService.getItems();
        this.finished = NewsService.getFinished();
        this.loadItems();
        angular.element(this._$window).bind("scroll", ::this.onScroll);
        $rootScope.$on("news-updated", ::this.onNewsUpdated);
        $rootScope.$on("news-created", ::this.onNewsCreated);
    }

    loadItems() {
        this.loading = true;
        let promise = this._NewsService.loadItems();
        promise && promise.then(() => {
            this.loading = false;
            this._$timeout(() => this._$scope.$apply());
        });
    }

    onScroll() {
        let el = this._$rootElement;
        let treshhold = 200; // run loading before scroll ends
        if ((el.offsetHeight + el.scrollTop + treshhold >= el.scrollHeight) && !this.loading && !this.finished) {
            this.loadItems();
        }
    }

    openDialog(item) {
        this._$rootScope.$emit('open-dialog', item);
    }

    onNewsUpdated(event, item) {
        this._NewsService.updateItem(item);
    }

    onNewsCreated(event, item) {
        this._NewsService.createItem(item);
    }

    deleteItem(id) {
        this._NewsService.deleteItem(id);
    }

}

export default contentController;